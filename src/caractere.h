#ifndef CARACTERE_H_
#define CARACTERE_H_
#include "liste_glyphe.h"
#include "matrix.h"

namespace std {

/**
 * \class Char
 * \brief Se comporte comme une pile de Glyphe, avec des fonctions d'affichage de ces derniers
 */
class Char {
private:
	Liste_Glyphe* glyphs; /*!< Pointeur vers le premier maillon de la liste de glyphes du caractère */
	int sizeG; /*!< Nombre de glyphes de glyphs */
public:
	/**
	 * \brief Constructeur par défaut de caractère
	 */
	Char();

	/**
	 * \brief Destructeur de caractère
	 */
	virtual ~Char();

	/**
	 * \brief Ajouter un glyphe au sommet de la pile
	 * \details Crée un maillon avec le glyphe g et l'ajoute au sommet de la pile
	 * \param g Le glyphe à rajouter
	 */
	void push(Glyphe g);

	/**
	 * \brief Retire le sommet de la pile
	 * \details Renvoie le premier maillon et le détruit
	 * \return Le glyphe du premier maillon de la pile
	 */
	Glyphe pop();

	/**
	 * \brief Dessine les glyphes dans une matrice
	 * \details Retire tous les glyphes un à un pour les afficher
	 */
	void draw(Matrix& m);

	/**
	 * \brief Dessine un glyphe dans une matrice
	 * \param g Le glyphe à dessiner
	 * \param m La matrice dans laquelle dessiner
	 */
	void casteljau(Glyphe g,Matrix& m) const;

	/**
	 * \brief La lettre A
	 */
	void A(Matrix& m);


	/**
	 * \brief La lettre A
	 */
	void B(Matrix& m);

	/**
	 * \brief La lettre C
	 */
	void C(Matrix& m);

	/**
	 * \brief La lettre D
	 */
	void D(Matrix& m);

	/**
	 * \brief La lettre E
	 */
	void E(Matrix& m);

	/**
	  * \brief La lettre E
	  */
	void F(Matrix& m);
};

}



#endif /* CARACTERE_H_ */
