#include "glyphe.h"

namespace std {

Glyphe::Glyphe(){}

Glyphe::Glyphe(Point& depart,Point& controle,Point& arrivee){
	points[0] = depart;
	points[1] = controle;
	points[2] = arrivee;
}

Glyphe::~Glyphe(){}

int Glyphe::test(){
	return (points[0].square_dist(points[1]) < 0.000001) && (points[1].square_dist(points[2]) < 0.000001);
}

}
