


#ifndef LIST_GLYPHE_H_
#define LIST_GLYPHE_H_
#include "glyphe.h"

/**
 * \file list_glyphe.h
 * \author Elise Farrugia, Thibault Lextrait
 * \details Ce fichier décrit la classe Liste_Glyphe et les méthodes pour les manipuler
 */
namespace std {

/**
 * \class Liste_Glyphe
 *
 * \brief Défini une structure de liste de glyphe
 */
class Liste_Glyphe{

private:
	Glyphe head; /*!< Le Glyphe du maillon */
	Liste_Glyphe* tail; /*!< Un pointeur vers le maillon suivant */
public:

	/**
	 * \brief Constructeur d'une liste à un élément
	 */
	Liste_Glyphe(Glyphe& g);

	/**
	 * \brief Constructeur de la classe Liste_Glyphe
	 */
	Liste_Glyphe(const Glyphe& g,Liste_Glyphe* next);

	/**
	 * \brief Getter de l'attribut head
	 *
	 * \return Le Glyphe du maillon
	 */
	Glyphe getHead() const{
		return head;
	}

	/**
	 * \brief Getter de l'attribut tail
	 *
	 * \return Le pointeur vers le maillon suivant
	 */
	Liste_Glyphe* getTail() const{
		return tail;
	}

	/**
	 * \brief Destructeur de la classe Liste_Glyphe
	 */
	virtual ~Liste_Glyphe();
};

}



#endif /* LIST_GLYPHE_H_ */
