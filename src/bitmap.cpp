/*
 * bitmap.cpp
 *
 *  Created on: 12 janv. 2019
 *      Author: thibault
 */

#include <iostream>
#include <cstdio>
#include <fstream>
#include "bitmap.h"


namespace std {


Bitmap::Bitmap(Matrix& m,char* path){
	data = m.getMatrix();
	sizeX = m.getSizeX();
	sizeY = m.getSizeY();
	this->path = path;
}

void Bitmap::CreateHeader(ofstream &bfile)
{
    int width = 256;                        //will be coded with 4 bytes
    int height = 256;                       //... 4 bytes

    int Type = 19778;                       //... 2 bytes
    int fSize = (width*height*1)+40+14+1024;  //... 4 bytes
    int Reserved1 = 0;                      //... 2 bytes
    int Reserved2 = 0;                      //... 2 bytes
    int Offset = 40+14+1024;                //... 4 bytes

    int hSize = 40;                         //... 4 bytes
    int nbPlane = 1;                        //... 2 bytes
    int nBPP = 8;                           //... 2 bytes
    int Compression = 0;                    //... 4 bytes
    int iSize = width*height*1;             //... 4 bytes
    int ResoX = 0;                          //... 4 bytes
    int ResoY = 0;                          //... 4 bytes
    int clrUsd = 256;                       //... 4 bytes
    int clrImp = 0;                         //... 4 bytes

    bfile.write((char*)&Type, 2);
    bfile.write((char*)&fSize, 4);
    bfile.write((char*)&Reserved1, 2);
    bfile.write((char*)&Reserved2, 2);
    bfile.write((char*)&Offset, 4);
    bfile.write((char*)&hSize, 4);
    bfile.write((char*)&width, 4);
    bfile.write((char*)&height, 4);
    bfile.write((char*)&nbPlane, 2);
    bfile.write((char*)&nBPP, 2);
    bfile.write((char*)&Compression, 4);
    bfile.write((char*)&iSize, 4);
    bfile.write((char*)&ResoX, 4);
    bfile.write((char*)&ResoY, 4);
    bfile.write((char*)&clrUsd, 4);
    bfile.write((char*)&clrImp, 4);
}

void Bitmap::CreatePalette(ofstream &bfile)
{
    int TabPalette[1024];
    int i=0;
    for (int x=0;x<256;x++)
    {
        TabPalette[i]=x;
        i++;
        TabPalette[i]=x;
        i++;
        TabPalette[i]=x;
        i++;
        TabPalette[i]=0;
        i++;
    }
    for (int x=0;x<1024;x++)
    {
        bfile.write((char*)&TabPalette[x], 1);
    }
}

void Bitmap::CreatePixel(ofstream &bfile)
{
    int white = 255;
    int grey = 127;
    int black = 0;
    int i,j;
    for (i=sizeX-1;i>=0;i--){
    	for (j=0;j<sizeY;j++){
    		switch (data[i][j]){
    			case 0:
    				bfile.write((char*)&white,1);
    				break;
    			case 1:
    				bfile.write((char*)&black,1);
    			    break;
    			case 2:
    				bfile.write((char*)&grey,1);
    				break;
    		}
    	}
    }
}
void Bitmap::CreateAndSaveBMP()
{
    ofstream bmpfile;
    bmpfile.open(path, ios::out | ios::binary | ios::trunc);
    if(!bmpfile.is_open())
    {
        cout << "ERROR: FILE COULD NOT BE OPENED:";
        cout << path << endl;
        return ;
    }
    CreateHeader(bmpfile);
    CreatePalette(bmpfile);
    CreatePixel(bmpfile);
    bmpfile.close();
}
}

