#ifndef POINT_H_
#define POINT_H_

#include <iostream>

namespace std{

/**
 * \class Point
 * \brief Représente un point aux coordonnées réelles, et les opérations pour le manipuler
 */
class Point
{
private:
	double x; /*!< Coordonnée x du point*/
	double y; /*!< Coordonnée y du point*/
public:
	/**
	 * \brief Constructeur de base classe Point
	 */
	Point();

	/**
	 * \brief Constructeur valué de la classe Point
	 * \param x Le paramètre x du point créé
	 * \param y Le paramètre y du point créé
	 */
	Point(double x,double y);

	/**
	 * \brief Getter de l'attribut x
	 *
	 * \return La valeur de la coordonnée x
	 */
	double getX() const;


	/**
	 * \brief Getter de l'attribut y
	 *
	 * \return La valeur de la coordonnée y
	 */
	double getY() const;

	/**
	 * \brief Destructeur de la classe Point
	 */
	virtual ~Point();

	/**
	 * \brief Calcule la distance avec un autre point
	 * \param a Un point
	 * \return Distance entre le point en argument et le point courant
	 */
	double square_dist(Point& a) const;

};

}

#endif /* POINT_H_ */
