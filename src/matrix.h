#ifndef MATRIX_H_
#define MATRIX_H_
#include "point.h"

namespace std {

/**
 * \class Matrix
 * \brief Ce fichier décrit la classe Point et les méthodes pour les manipuler
 */
class Matrix{
private:
	int sizeX; /*!< Nombre de lignes de la matrice */
	int sizeY; /*!< Nombre de colonnes de la matrice */
	int** data; /*!< Données de la matrice */
public:

	/**
	 * \brief Construit une matrice de 0 de taille x,y
	 * \param x Le nombre de lignes
	 * \param y Le nombre de colonnes
	 */
	Matrix(int x,int y);

	/**
	 * \brief Assigner une valeur à un point de la matrice
	 *
	 * \param x Coordonnée x du point à modifier
	 * \param y Coordonnée y du point à modifier
	 * \param value Valeur à affecter
	 * \attention 0<=x<size, 0<=y<sizeY
	 */
	void set(int x,int y,int value);

	/**
	 * \brief Assigner une valeur à un point de la matrice
	 *
	 * \param p Point à modifier
	 * \param value Valeur à affecter
	 */
	void setPoint(Point& p,int value);

	/**
	 * \brief Getter de l'attribut sizeX
	 *
	 * \return La valeur de sizeX
	 */
	int getSizeX() const{
		return sizeX;
	}

	/**
	 * \brief Getter de l'attribut sizeX
	 *
	 * \return La valeur de sizeX
	 */
	int getSizeY() const{
		return sizeY;
	}

	/**
	 * \brief Getter de l'attribut data
	 *
	 * \return La matrice de la classe
	 */
	int** getMatrix() const{
		return data;
	}

	/**
	 * \brief Destructeur de la classe Matrix
	 */
	virtual ~Matrix();

	/**
	 * \brief Affiche la matrice courante
	 */
	void print() const;

	/**
	 * \brief Remplit une lettre
	 * \details De proche en proche affecte à 1 les points de la matrice
	 * jusqu'à trouver un point déjà à 1
	 * \param x L'abscisse du point de départ
	 * \param y L'ordonnée du point de départ
	 * \attention 0<=x<size, 0<=y<sizeY
	 */
	void fill(int x,int y);

	/**
	 * \brief Entoure une lettre
	 * \details Pour chaque point de valeur 1, affecte à 2
	 * ses proches voisins de valeur 0.
	 */
	void lining();
};

}



#endif /* MATRIX_H_ */
