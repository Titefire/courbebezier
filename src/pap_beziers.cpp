#include <stdio.h>
#include <stdlib.h>
#include "caractere.h"
#include "matrix.h"
#include "liste_glyphe.h"
#include "bitmap.h"

using namespace std;

int main(void) {
	Char * c = new Char();

	/* Pour A */
	Matrix* ma = new Matrix(256,256);
	c->A(*ma); /* On enregistre les glyphes nécessaires pour dessiner A */
	c->draw(*ma); /* On dessine A dans la matrice */
	char A1[] = "../Bitmap/A_police1.bmp"; /* Chemin du bitmap créé */
	Bitmap* ba1= new Bitmap(*ma,A1); /* On crée le bitmap à partir de la matrice */
	ba1->CreateAndSaveBMP(); /* On enregistre le bitmap dans un fichiers .bmp */
	ma->fill((85*ma->getSizeX()/100),(15*ma->getSizeY()/100)); /* On remplit le A dans la matrice */
	char A2[] = "../Bitmap/A_police2.bmp";
	Bitmap* ba2= new Bitmap(*ma,A2);
	ba2->CreateAndSaveBMP();
	ma->lining(); /* On crée les contours de A dans la matrice */
	char A3[] = "../Bitmap/A_police3.bmp";
	Bitmap* ba3= new Bitmap(*ma,A3);
	ba3->CreateAndSaveBMP();
	/*ma->print();*/ /* Affiche une matrice dans le terminal linux (0 pour blanc, 1 pour noir, 2 pour rouge) */


	/* Pour B */
	Matrix* mb = new Matrix(256,256);
	c->B(*mb);
	c->draw(*mb);
	char B1[] = "../Bitmap/B_police1.bmp";
	Bitmap* bb1= new Bitmap(*mb,B1);
	bb1->CreateAndSaveBMP();
	mb->fill((10*mb->getSizeX()/100),(10*mb->getSizeY()/100));
	char B2[] = "../Bitmap/B_police2.bmp";
	Bitmap* bb2= new Bitmap(*mb,B2);
	bb2->CreateAndSaveBMP();
	mb->lining();
	char B3[] = "../Bitmap/B_police3.bmp";
	Bitmap* bb3= new Bitmap(*mb,B3);
	bb3->CreateAndSaveBMP();
	/*mb->print();*/ /* Affiche une matrice dans le terminal linux (0 pour blanc, 1 pour noir, 2 pour rouge) */

	/* Pour C */
	Matrix* mc = new Matrix(256,256);
	c->C(*mc);
	c->draw(*mc);
	char C1[] = "../Bitmap/C_police1.bmp";
	Bitmap* bc1= new Bitmap(*mc,C1);
	bc1->CreateAndSaveBMP();
	mc->fill((13*mc->getSizeX()/100),(60*mc->getSizeY()/100));
	char C2[] = "../Bitmap/C_police2.bmp";
	Bitmap* bc2= new Bitmap(*mc,C2);
	bc2->CreateAndSaveBMP();
	mc->lining();
	char C3[] = "../Bitmap/C_police3.bmp";
	Bitmap* bc3= new Bitmap(*mc,C3);
	bc3->CreateAndSaveBMP();
	/*mc->print();*/ /* Affiche une matrice dans le terminal linux (0 pour blanc, 1 pour noir, 2 pour rouge) */

	/* Pour D */
	Matrix* md = new Matrix(256,256);
	c->D(*md);
	c->draw(*md);
	char D1[] = "../Bitmap/D_police1.bmp";
	Bitmap* bd1= new Bitmap(*md,D1);
	bd1->CreateAndSaveBMP();
	md->fill((6*md->getSizeX()/100),(6*md->getSizeY()/100));
	char D2[] = "../Bitmap/D_police2.bmp";
	Bitmap* bd2= new Bitmap(*md,D2);
	bd2->CreateAndSaveBMP();
	md->lining();
	char D3[] = "../Bitmap/D_police3.bmp";
	Bitmap* bd3= new Bitmap(*md,D3);
	bd3->CreateAndSaveBMP();
	/*md->print();*/ /* Affiche une matrice dans le terminal linux (0 pour blanc, 1 pour noir, 2 pour rouge) */

	/* Pour E */
	Matrix* me = new Matrix(256,256);
	c->E(*me);
	c->draw(*me);
	char E1[] = "../Bitmap/E_police1.bmp";
	Bitmap* be1= new Bitmap(*me,E1);
	be1->CreateAndSaveBMP();
	me->fill((6*me->getSizeX()/100),(6*me->getSizeY()/100));
	char E2[] = "../Bitmap/E_police2.bmp";
	Bitmap* be2= new Bitmap(*me,E2);
	be2->CreateAndSaveBMP();
	me->lining();
	char E3[] = "../Bitmap/E_police3.bmp";
	Bitmap* be3= new Bitmap(*me,E3);
	be3->CreateAndSaveBMP();
	/*me->print();*/ /* Affiche une matrice dans le terminal linux (0 pour blanc, 1 pour noir, 2 pour rouge) */

	/* Pour F */
	Matrix* mf = new Matrix(256,256);
	c->F(*mf);
	c->draw(*mf);
	char F1[] = "../Bitmap/F_police1.bmp";
	Bitmap* bf1= new Bitmap(*mf,F1);
	bf1->CreateAndSaveBMP();
	mf->fill((6*mf->getSizeX()/100),(6*mf->getSizeY()/100));
	char F2[] = "../Bitmap/F_police2.bmp";
	Bitmap* bf2= new Bitmap(*mf,F2);
	bf2->CreateAndSaveBMP();
	mf->lining();
	char F3[] = "../Bitmap/F_police3.bmp";
	Bitmap* bf3= new Bitmap(*mf,F3);
	bf3->CreateAndSaveBMP();
	/*mf->print();*/ /* Affiche une matrice dans le terminal linux (0 pour blanc, 1 pour noir, 2 pour rouge) */

	return 0;
}
