#include "matrix.h"

namespace std{

Matrix::Matrix(int x,int y){
	this->sizeX = x;
	this->sizeY = y;
	this->data = new int* [sizeX];
	int i;
	for(i=0; i<sizeX; i++){
		this->data[i] = new int [sizeY];
	}
}

void Matrix::set(int x,int y,int value){
	data[x][y] = value;
}

void Matrix::setPoint(Point& p,int value){
	int i = (int)p.getX();
	int j = (int)p.getY();
	set(i,j,value);
}


Matrix::~Matrix(){

}

void Matrix::print() const{
	int i,j;
	for (i=0;i<sizeX;i++){
		for (j=0;j<sizeY;j++){
			cout << data[i][j];
		}
		cout << endl;
	}
}

void Matrix::fill(int x,int y){
	set(x,y,1);
	if (x-1>=0 && data[x-1][y]!=1){
		fill(x-1,y);
	}
	if (y-1>=0 && data[x][y-1]!=1){
			fill(x,y-1);
		}
	if (x+1<sizeX && data[x+1][y]!=1){
			fill(x+1,y);
		}
	if (y+1<sizeY && data[x][y+1]!=1){
			fill(x,y+1);
		}
}

void Matrix::lining(){
	int i,j;
	for (i=0;i<sizeX;i++){
		for(j=0;j<sizeY;j++){
			if (data[i][j]==1){
				if(i+1 < sizeX && data[i+1][j]==0) set(i+1,j,2);
				if(i+2 < sizeX && data[i+2][j]==0) set(i+2,j,2);
				if(i-1 >= 0 && data[i-1][j]==0) set(i-1,j,2);
				if(i-2 >= 0 && data[i-2][j]==0) set(i-2,j,2);
				if(j+1 < sizeY && data[i][j+1]==0) set(i,j+1,2);
				if(j+2 < sizeY && data[i][j+2]==0) set(i,j+2,2);
				if(j-1 >= 0 && data[i][j-1]==0) set(i,j-1,2);
				if(j-2 >= 0 && data[i][j-2]==0) set(i,j-2,2);
			}
		}
	}
}

}


