#include "point.h"

namespace std{

Point::Point(){
	this->x=0.0;
	this->y=0.0;
}

Point::Point(double x,double y){
	this->x=x;
	this->y=y;
}

Point::~Point()
{}

double Point::getX() const {
	return x;
}

double Point::getY() const {
	return y;
}

double Point::square_dist(Point& a) const {
	return ((getX()-a.getX())*(getX()-a.getX())+(getY()-a.getY())*(getY()-a.getY()));
}

}
