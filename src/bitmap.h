#ifndef BITMAP_H_
#define BITMAP_H_

#include <fstream>
#include "matrix.h"

namespace std{

/**
 * \class Bitmap
 * \brief Classe pour créer des fichiers bmp à partir d'une matrice
 */
class Bitmap
{
private:
	int** data; /*!< La matrice de à afficher */
	int sizeX; /*!< Le nombre de lignes de la matrice */
	int sizeY; /*!< Le nombre de colonnes de la matrice */
	char* path; /*!< Le nom sous lequel enregistrer  le fichier */
public:

	/**
	 * \brief Constructeur de la classe Bitmap
	 * \param m La matrice à afficher
	 * \param path Le chemin auquel enregister le bitmap
	 */
	Bitmap(Matrix& m,char* path);

	/**
	 * \brief Sauvegarde le bitmap dans le fichier path
	 */
	void CreateAndSaveBMP();

	/**
	 * \brief Crée le header d'un fichier bitmap
	 */
	void CreateHeader(ofstream &bfile);

	/**
	 * \brief Crée la palette d'un fichier bitmap
	 */
	void CreatePalette(ofstream &bfile);

	/**
	 * \brief Remplit le fichier bitmap avec les pixels pris dans la matrice
	 */
	void CreatePixel(ofstream &bfile);

};

}

#endif /* BITMAP_H_ */
