#include "liste_glyphe.h"
#include <cstdlib>

namespace std {

Liste_Glyphe::Liste_Glyphe(Glyphe& g){
	this->head = g;
	this->tail = NULL;
}

Liste_Glyphe::Liste_Glyphe(const Glyphe& g,Liste_Glyphe*next){
	this->head = g;
	this->tail = next;
}

Liste_Glyphe::~Liste_Glyphe() {
}

}

