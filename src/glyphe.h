
#ifndef GLYPHE_H_
#define GLYPHE_H_
#include "point.h"

namespace std {

/**
 * \class Glyphe
 * \brief Représente un glyphe et les opérations pour le manipuler
 */
class Glyphe {
private:
	Point points[3]; /*!< L'ensemble de trois points définissant le glyphe */
public:
	/**
	 * \brief Constructeur de base classe Glyphe
	 */
	Glyphe();

	/**
	 * \brief Constructeur valué de la classe Graphe
	 * \param depart Le point appartenant à la première courbe
	 * \param controle Le point de contrôle de la courbe de Bézier
	 * \param arrivee Le point appartenant à la seconde courbe
	 */
	Glyphe(Point& depart,Point& point_controle,Point& arrivee);

	/**
	 * \brief Destructeur de la classe Glyphe
	 */
	virtual ~Glyphe();

	/**
	 * \brief Getter de l'attribut points
	 *
	 * \return Les trois points de contrôles
	 */
	const Point* getPoints() const{
		return points;
	}

	/**
	 * \brief Teste si le point de contrôle est une extrêmité
	 * Vérifie que la distance entre le point de contrôle et les extrêmités sont non nulles.
	 * Étant donné qu'on travaille avec des flottants il peut y avoir des erreurs d'arrondis.
	 */
	int test();

};

}

#endif /* GLYPHE_H_ */
