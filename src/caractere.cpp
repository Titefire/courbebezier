#include "caractere.h"

namespace std{

Char::Char(){
	glyphs=NULL;
	sizeG=0;
}

Char::~Char(){

}

void Char::push(Glyphe g){
	Liste_Glyphe* node = new Liste_Glyphe(g,glyphs);
	glyphs = node;
	sizeG++;
}

Glyphe Char::pop(){
	Glyphe res = glyphs->getHead();
	if (sizeG>0){
		Liste_Glyphe* node = glyphs;
		glyphs=glyphs->getTail();
		delete node;
		sizeG--;
	}
	return res;
}

void Char::casteljau(Glyphe g,Matrix& m) const{
	if (!g.test()){
		Point T[3][3]; /* Matrice de 3*3points */
		int i,j;
		double x,y;
		/* On remplit la première ligne avec les points de contrôles */
		for(i = 0; i < 3; i++)
			{
				T[0][i] = g.getPoints()[i];
			}
		/* Pour chaque nouvelle ligne, T[i][j] définit le point équidistant entre T[i-1][j] et T[i-1][j+1]
		 * C'est à dire entre les deux points situés au-dessus dans la matrice
		 */
			for(i = 1; i < 3; i++)
			{
				for(j = 0; j < (3-i); j++)
				{
					x = (T[i-1][j].getX()+T[i-1][j+1].getX())/2;
					y = (T[i-1][j].getY()+T[i-1][j+1].getY())/2;
					T[i][j] = Point(x,y);
				}
			}
			m.setPoint(T[2][0],1);
			/* On réapplique l'algorithme sur:
			 * le point de départ, le milieu entre le point de départ et le point de controle, le point de la courbe créé
			 * le point de la courbe créé, le milieu entre le point de contrôle et le point d'arrivée, le point d'arrivée
			 */
			Glyphe * g1 = new Glyphe(T[0][0], T[1][0], T[2][0]);
			Glyphe * g2 = new Glyphe(T[2][0], T[1][1], T[0][2]);
			casteljau(*g1,m);
			casteljau(*g2,m);
	}
}

void Char::draw(Matrix& m){
	while (sizeG>0){
		casteljau(pop(),m);
	}
}

/*
        g
       / \
      / i \
     / / \ \
    / h__j  \
   /         \
  /  c____d   \
 /   /	   \   \
a___b       e__f

 */
void Char::A(Matrix& m) {
	int x = m.getSizeX();
	int y = m.getSizeY();
	Point * a = new Point((90.0 * x)/100.0,(10.0*y)/100.0);
	Point * g = new Point((10.0 * x)/100.0,(50.0*y)/100.0);
	Point * ag = new Point((50.0*x)/100.0,(30.0*y)/100.0);
	Glyphe * g1 = new Glyphe(*a, *ag, *g);

	Point * f = new Point((90.0 * x)/100.0,(90.0*y)/100.0);
	Point * fg = new Point((50.0*x)/100.0,(70.0*y)/100.0);
	Glyphe * g2 = new Glyphe(*f, *fg, *g);

	Point * b = new Point((90.0 * x)/100.0,(30.0*y)/100.0);
	Point * ab = new Point((90.0*x)/100.0,(20.0*y)/100.0);
	Glyphe * g3 = new Glyphe(*a, *ab, *b);

	Point * e = new Point((90.0 * x)/100.0,(70.0*y)/100.0);
	Point * ef = new Point((90.0*x)/100.0,(80.0*y)/100.0);
	Glyphe * g4 = new Glyphe(*f, *ef, *e);

	Point * c = new Point((70.0 * x)/100.0,(40.0*y)/100.0);
	Point * bc = new Point((80.0*x)/100.0,(35.0*y)/100.0);
	Glyphe * g5 = new Glyphe(*b, *bc, *c);

	Point * d = new Point((70.0 * x)/100.0,(60.0*y)/100.0);
	Point * ed = new Point((80.0*x)/100.0,(65.0*y)/100.0);
	Glyphe * g6 = new Glyphe(*e, *ed, *d);

	Point * cd = new Point((70.0*x)/100.0,(50.0*y)/100.0);
	Glyphe * g7 = new Glyphe(*c, *cd, *d);

	Point * h = new Point((60.0 * x)/100.0,(45.0*y)/100.0);
	Point * i = new Point((30.0 * x)/100.0,(50.0*y)/100.0);
	Point * j = new Point((60.0 * x)/100.0,(55.0*y)/100.0);
	Point * ij = new Point((65.0 * x)/100.0,(52.5*y)/100.0);
	Point * hi = new Point((65.0 * x)/100.0,(47.5*y)/100.0);
	Point * hj = new Point((60.0 * x)/100.0,(50.0*y)/100.0);
	Glyphe * g8 = new Glyphe(*h, *hi, *i);
	Glyphe * g9 = new Glyphe(*i, *ij, *j);
	Glyphe * g10 = new Glyphe(*h, *hj, *j);


	push(*g1);
	push(*g2);
	push(*g3);
	push(*g4);
	push(*g5);
	push(*g6);
	push(*g7);
	push(*g8);
	push(*g9);
	push(*g10);
}



/*
b____________
|		       \
|    c \          h
|    |  \       /
|    d /      /
|         	g
|             \
|    e \        \
|    |  \         i
|    f /         /
|               /
a______________/

 */


void Char::B(Matrix& m) {
	int x = m.getSizeX();
	int y = m.getSizeY();
	Point * a = new Point((95.0 * x)/100.0,(5.0*y)/100.0);
	Point * b = new Point((5.0 * x)/100.0,(5.0*y)/100.0);
	Point * ab = new Point((50.0 * x)/100.0,(5.0*y)/100.0);
	Point * c = new Point((20.0 * x)/100.0,(25.0*y)/100.0);
	Point * d = new Point((35.0 * x)/100.0,(25.0*y)/100.0);
	Point * cd = new Point((27.5 * x)/100.0,(25.0*y)/100.0);
	Point * e = new Point((65.0 * x)/100.0,(25.0*y)/100.0);
	Point * f = new Point((80.0 * x)/100.0,(25.0*y)/100.0);
	Point * ef = new Point((72.5 * x)/100.0,(25.0*y)/100.0);
	Point * g = new Point((50.0 * x)/100.0,(40.0*y)/100.0);
	Point * h = new Point((27.5 * x)/100.0,(70.0*y)/100.0);
	Point * i = new Point((72.5 * x)/100.0,(80.0*y)/100.0);
	Point * bh = new Point((0.0 * x)/100.0,(80.0*y)/100.0);
	Point * hg = new Point((40.0 * x)/100.0,(70.0*y)/100.0);
	Point * gi = new Point((60.0 * x)/100.0,(70.0*y)/100.0);
	Point * ai = new Point((100.0 * x)/100.0,(80.0*y)/100.0);
	Point * cd2 = new Point((27.5 * x)/100.0,(70.0*y)/100.0);
	Point * ef2 = new Point((75.0 * x)/100.0,(80.0*y)/100.0);
	Glyphe* g1 = new Glyphe(*a, *ab, *b);
	Glyphe* g2 = new Glyphe(*c, *cd, *d);
	Glyphe* g3 = new Glyphe(*e, *ef, *f);
	Glyphe* g4 = new Glyphe(*b,*bh,*h);
	Glyphe* g5 = new Glyphe(*h,*hg,*g);
	Glyphe* g6 = new Glyphe(*g,*gi,*i);
	Glyphe* g7 = new Glyphe(*a,*ai,*i);
	Glyphe* g8 = new Glyphe(*c,*cd2,*d);
	Glyphe* g9 = new Glyphe(*e,*ef2,*f);

	push(*g1);
	push(*g2);
	push(*g3);
	push(*g4);
	push(*g5);
	push(*g6);
	push(*g7);
	push(*g8);
	push(*g9);
}

void Char::C(Matrix& m) {
	int x = m.getSizeX();
	int y = m.getSizeY();
	Point * a = new Point((8.0 * x)/100.0,(73.0*y)/100.0);
	Point * r = new Point((50.0 * x)/100.0,(7.0*y)/100.0);
	Point * c = new Point((8.0*x)/100.0,(7.0*y)/100.0);
	Glyphe * g = new Glyphe(*a, *c, *r);

	Point * a1 = new Point((50.0*x)/100.0,(7.0*y)/100.0);
	Point * r1 = new Point((92.0*x)/100.0,(73.0*y)/100.0);
	Point * c1 = new Point((92.0*x)/100.0,(7.0*y)/100.0);
	Glyphe * g2 = new Glyphe(*a1,*c1,*r1);

	Point * a2 = new Point((17.0*x)/100.0,(73.0*y)/100.0);
	Point * r2 = new Point((50.0*x)/100.0,(20.0*y)/100.0);
	Point * c2 = new Point((17.0*x)/100.0,(13.0*y)/100.0);
	Glyphe * g3 = new Glyphe(*a2,*c2,*r2);

	Point * a3 = new Point((50.0*x)/100.0,(20.0*y)/100.0);
	Point * r3 = new Point((83.0*x)/100.0,(73.0*y)/100.0);
	Point * c3 = new Point((92.0*x)/100.0,(13.0*y)/100.0);
	Glyphe * g4 = new Glyphe(*a3,*c3,*r3);

	Point * a4 = new Point((8.0*x)/100.0,(73.0*y)/100.0);
	Point * r4 = new Point((17.0*x)/100.0,(73.0*y)/100.0);
	Point * c4 = new Point((12.5*x)/100.0,(73.0*y)/100.0);
	Glyphe * g5 = new Glyphe(*a4,*c4,*r4);

	Point * a5 = new Point((83.0*x)/100.0,(73.0*y)/100.0);
	Point * r5 = new Point((92.0*x)/100.0,(73.0*y)/100.0);
	Point * c5 = new Point((87.5*x)/100.0,(73.0*y)/100.0);
	Glyphe * g6 = new Glyphe(*a5,*c5,*r5);

	push(*g);
	push(*g2);
	push(*g3);
	push(*g4);
	push(*g5);
	push(*g6);
}


/*

a_____
|      \
|  c    \
|  | \   \
|  | /   /
|  d-   /
b_____ /




 */



void Char::D(Matrix& m){
	int x = m.getSizeX();
	int y = m.getSizeY();
	Point * a = new Point((5.0*x)/100.0,(5.0*y)/100.0);
	Point * b = new Point((95.0*x)/100.0,(5.0*y)/100.0);
	Point * c = new Point((25.0*x)/100.0,(30.0*y)/100.0);
	Point * d = new Point((75.0*x)/100.0,(30.0*y)/100.0);
	Point * ab = new Point((50.0*x)/100.0,(5.0*y)/100.0);
	Point * cd = new Point((50.0*x)/100.0,(30.0*y)/100.0);
	Glyphe* g1 = new Glyphe(*a,*ab,*b);
	Glyphe* g2 = new Glyphe(*c,*cd,*d);
	Point * h = new Point((50.0*x)/100.0,(95.0*y)/100.0);
	Point * i = new Point((50.0*x)/100.0,(60.0*y)/100.0);
	Point * ah = new Point((5.0*x)/100.0,(100.0*y)/100.0);
	Point * bh = new Point((95.0*x)/100.0,(100.0*y)/100.0);
	Point * ci = new Point((25.0*x)/100.0,(60.0*y)/100.0);
	Point * di = new Point((75.0*x)/100.0,(60.0*y)/100.0);
	Glyphe* g3 = new Glyphe(*a,*ah,*h);
	Glyphe* g4 = new Glyphe(*b,*bh,*h);
	Glyphe* g5 = new Glyphe(*c,*ci,*i);
	Glyphe* g6 = new Glyphe(*d,*di,*i);

	push(*g1);
	push(*g2);
	push(*g3);
	push(*g4);
	push(*g5);
	push(*g6);
}

void Char::E(Matrix& m){
	int x = m.getSizeX();
	int y = m.getSizeY();
	Point * a = new Point((5.0*x)/100.0,(5.0*y)/100.0);
	Point * b = new Point((5.0*x)/100.0,(95.0*y)/100.0);
	Point * c = new Point((23.0*x)/100.0,(95.0*y)/100.0);
	Point * d = new Point((23.0*x)/100.0,(35.0*y)/100.0);
	Point * e = new Point((41.0*x)/100.0,(35.0*y)/100.0);
	Point * f = new Point((41.0*x)/100.0,(70.0*y)/100.0);
	Point * g = new Point((59.0*x)/100.0,(70.0*y)/100.0);
	Point * h = new Point((59.0*x)/100.0,(35.0*y)/100.0);
	Point * i = new Point((77.0*x)/100.0,(35.0*y)/100.0);
	Point * j = new Point((77.0*x)/100.0,(95.0*y)/100.0);
	Point * k = new Point((95.0*x)/100.0,(95.0*y)/100.0);
	Point * l = new Point((95.0*x)/100.0,(5.0*y)/100.0);
	Point * ab = new Point((5.0*x)/100.0,(50.0*y)/100.0);
	Point * bc = new Point((14.0*x)/100.0,(95.0*y)/100.0);
	Point * cd = new Point((23.0*x)/100.0,(65.0*y)/100.0);
	Point * de = new Point((32.0*x)/100.0,(35.0*y)/100.0);
	Point * ef = new Point((41.0*x)/100.0,(52.5*y)/100.0);
	Point * fg = new Point((50.0*x)/100.0,(70.0*y)/100.0);
	Point * gh = new Point((59.0*x)/100.0,(52.5*y)/100.0);
	Point * hi = new Point((68.0*x)/100.0,(35.0*y)/100.0);
	Point * ij = new Point((77.0*x)/100.0,(65.0*y)/100.0);
	Point * jk = new Point((86.0*x)/100.0,(95.0*y)/100.0);
	Point * kl = new Point((95.0*x)/100.0,(50.0*y)/100.0);
	Point * la = new Point((50.0*x)/100.0,(5.0*y)/100.0);
	Glyphe* g1 = new Glyphe(*a,*ab,*b);
	Glyphe* g2 = new Glyphe(*b,*bc,*c);
	Glyphe* g3 = new Glyphe(*c,*cd,*d);
	Glyphe* g4 = new Glyphe(*d,*de,*e);
	Glyphe* g5 = new Glyphe(*e,*ef,*f);
	Glyphe* g6 = new Glyphe(*f,*fg,*g);
	Glyphe* g7 = new Glyphe(*g,*gh,*h);
	Glyphe* g8 = new Glyphe(*h,*hi,*i);
	Glyphe* g9 = new Glyphe(*i,*ij,*j);
	Glyphe* g10 = new Glyphe(*j,*jk,*k);
	Glyphe* g11 = new Glyphe(*k,*kl,*l);
	Glyphe* g12 = new Glyphe(*l,*la,*a);

	push(*g1);
	push(*g2);
	push(*g3);
	push(*g4);
	push(*g5);
	push(*g6);
	push(*g7);
	push(*g8);
	push(*g9);
	push(*g10);
	push(*g11);
	push(*g12);
}

void Char::F(Matrix& m){
	int x = m.getSizeX();
	int y = m.getSizeY();
	Point * a = new Point((5.0*x)/100.0,(5.0*y)/100.0);
	Point * b = new Point((5.0*x)/100.0,(95.0*y)/100.0);
	Point * c = new Point((23.0*x)/100.0,(95.0*y)/100.0);
	Point * d = new Point((23.0*x)/100.0,(35.0*y)/100.0);
	Point * e = new Point((41.0*x)/100.0,(35.0*y)/100.0);
	Point * f = new Point((41.0*x)/100.0,(70.0*y)/100.0);
	Point * g = new Point((59.0*x)/100.0,(70.0*y)/100.0);
	Point * h = new Point((59.0*x)/100.0,(35.0*y)/100.0);
	Point * k = new Point((95.0*x)/100.0,(35.0*y)/100.0);
	Point * l = new Point((95.0*x)/100.0,(5.0*y)/100.0);
	Point * ab = new Point((5.0*x)/100.0,(50.0*y)/100.0);
	Point * bc = new Point((14.0*x)/100.0,(95.0*y)/100.0);
	Point * cd = new Point((23.0*x)/100.0,(65.0*y)/100.0);
	Point * de = new Point((32.0*x)/100.0,(35.0*y)/100.0);
	Point * ef = new Point((41.0*x)/100.0,(52.5*y)/100.0);
	Point * fg = new Point((50.0*x)/100.0,(70.0*y)/100.0);
	Point * gh = new Point((59.0*x)/100.0,(52.5*y)/100.0);
	Point * hk = new Point((77.0*x)/100.0,(35.0*y)/100.0);
	Point * kl = new Point((95.0*x)/100.0,(20.0*y)/100.0);
	Point * la = new Point((50.0*x)/100.0,(5.0*y)/100.0);
	Glyphe* g1 = new Glyphe(*a,*ab,*b);
	Glyphe* g2 = new Glyphe(*b,*bc,*c);
	Glyphe* g3 = new Glyphe(*c,*cd,*d);
	Glyphe* g4 = new Glyphe(*d,*de,*e);
	Glyphe* g5 = new Glyphe(*e,*ef,*f);
	Glyphe* g6 = new Glyphe(*f,*fg,*g);
	Glyphe* g7 = new Glyphe(*g,*gh,*h);
	Glyphe* g8 = new Glyphe(*h,*hk,*k);
	Glyphe* g11 = new Glyphe(*k,*kl,*l);
	Glyphe* g12 = new Glyphe(*l,*la,*a);

	push(*g1);
	push(*g2);
	push(*g3);
	push(*g4);
	push(*g5);
	push(*g6);
	push(*g7);
	push(*g8);
	push(*g11);
	push(*g12);
}

}



