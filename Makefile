CXXO =	-O2 -g -Wall -fmessage-length=0 -c
CXX = g++

OBJS = $(OBJDIR)/point.o $(OBJDIR)/glyphe.o $(OBJDIR)/matrix.o $(OBJDIR)/liste_glyphe.o $(OBJDIR)/caractere.o $(OBJDIR)/bitmap.o 

all: compile doc rapport


OBJDIR = Objets
BITDIR = Bitmap
SRCDIR = src
DOCDIR = Documentation
BINDIR = Executable

$(BINDIR)/pap_beziers: $(OBJS) $(OBJDIR)/pap_beziers.o 
	$(CXX) $^ -o  $@

$(OBJDIR)/point.o: $(SRCDIR)/point.cpp
	$(CXX) $(CXXO) $< -o $@

$(OBJDIR)/glyphe.o: $(SRCDIR)/glyphe.cpp
	$(CXX) $(CXXO) $< -o $@

$(OBJDIR)/matrix.o: $(SRCDIR)/matrix.cpp
	$(CXX) $(CXXO) $< -o $@

$(OBJDIR)/liste_glyphe.o: $(SRCDIR)/liste_glyphe.cpp
	$(CXX) $(CXXO) $< -o $@

$(OBJDIR)/caractere.o: $(SRCDIR)/caractere.cpp
	$(CXX) $(CXXO) $< -o $@

$(OBJDIR)/bitmap.o: $(SRCDIR)/bitmap.cpp
	$(CXX) $(CXXO) $< -o $@

$(OBJDIR)/pap_beziers.o : $(SRCDIR)/pap_beziers.cpp
	$(CXX) $(CXXO) $< -o $@


compile: $(BINDIR)/pap_beziers
	@cd $(BINDIR); ./pap_beziers

doc:
		@cd $(DOCDIR); doxygen Doxyfile ; cd latex ; make

rapport:
	cd $(DOCDIR)/Rapport ; pdflatex projet.tex

clean:
	rm -r $(OBJDIR)/*.o $(BITDIR)/*.bmp $(BINDIR)/*.bmp $(BINDIR)/pap_beziers $(DOCDIR)/latex $(DOCDIR)/html $(DOCDIR)/Rapport/projet.aux $(DOCDIR)/Rapport/projet.toc $(DOCDIR)/Rapport/projet.log $(DOCDIR)/Rapport/projet.pdf
