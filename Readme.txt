Le projet est séparé en 5 répertoires et un Makefile :

Bitmap: les fichiers .bmp seront générés ici
Documentation: la documentation doxygen, et le dossier pour compiler le rapport
Executable: l'executable obtenu par compilation est généré ici
Objets: les fichiers objets obtenus par précompilation
src: les fichiers de code

La commande make permet de générer le code, l'exécutable et les fichiers .bmp, cela compile également le code LaTeX du rapport et de la documentation doxygen.
Vous obtiendrez donc les images et la documentation.


La commande make clean permet de supprimer la documentation générée, l'exécutable, les objets et les fichiers .bmp.

Il est préférable d'exécuter l'exécutable depuis son répertoire ou via la commande make.

L'exécutable Executable/exec est une version opérationnelle du code (au cas où une erreur surviendrait à la compilation sur divers machines)
De même Documentation/Rapport_PAP.pdf et Documentation/Manuel.pdf sont des versions compilées de documentation.
